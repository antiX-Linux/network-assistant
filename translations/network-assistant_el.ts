<?xml version="1.0" ?><!DOCTYPE TS><TS language="el" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <location filename="ui_mainwindow.h" line="756"/>
        <source>Network Assistant</source>
        <translation>Βοηθός δικτύου</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="70"/>
        <location filename="ui_mainwindow.h" line="770"/>
        <source>Status</source>
        <translation>Κατάσταση:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="76"/>
        <location filename="ui_mainwindow.h" line="757"/>
        <source>IP address</source>
        <translation>Διεύθυνση IP</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="112"/>
        <location filename="ui_mainwindow.h" line="760"/>
        <source>Hardware detected</source>
        <translation>ανιχνεύθηκε το υλικό</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="143"/>
        <location filename="mainwindow.ui" line="389"/>
        <location filename="mainwindow.ui" line="502"/>
        <location filename="ui_mainwindow.h" line="761"/>
        <location filename="ui_mainwindow.h" line="774"/>
        <location filename="ui_mainwindow.h" line="780"/>
        <source>Re-scan</source>
        <translation>επαν-σάρωσης</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="215"/>
        <location filename="ui_mainwindow.h" line="762"/>
        <source>Active interface</source>
        <translation>Ενεργή διασύνδεση</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="231"/>
        <location filename="ui_mainwindow.h" line="764"/>
        <source>WiFi status</source>
        <translation>Κατάσταση WiFi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="253"/>
        <location filename="ui_mainwindow.h" line="767"/>
        <source>Unblocks all soft/hard blocked wireless devices</source>
        <translation>Ξεμπλοκάρει όλα τα μαλακά/σκληρά μπλοκάρεσμενα Ασύρματες Συσκευές</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="256"/>
        <location filename="ui_mainwindow.h" line="769"/>
        <source>Unblock WiFi Devices</source>
        <translation>Ξεμπλοκάρει Συσκευές WIFI</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="288"/>
        <location filename="ui_mainwindow.h" line="776"/>
        <source>Linux drivers</source>
        <translation>οδηγοί του Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="300"/>
        <location filename="ui_mainwindow.h" line="771"/>
        <source>Associated Linux drivers</source>
        <translation>Associated Linux drivers</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="309"/>
        <location filename="ui_mainwindow.h" line="772"/>
        <source>Load Driver</source>
        <translation>Φορτώση του Driver</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="372"/>
        <location filename="ui_mainwindow.h" line="773"/>
        <source>Unload Driver</source>
        <translation>Ξεφορτώση του Driver</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="409"/>
        <location filename="ui_mainwindow.h" line="775"/>
        <source>Block Driver</source>
        <translation>Αποκλεισμός του driver</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="428"/>
        <location filename="ui_mainwindow.h" line="785"/>
        <source>Windows drivers</source>
        <translation>οδηγοί του Windows</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="434"/>
        <location filename="ui_mainwindow.h" line="777"/>
        <source>Available Windows drivers</source>
        <translation>Διαθέσιμα drivers του Windows</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="468"/>
        <location filename="ui_mainwindow.h" line="778"/>
        <source>Remove Driver</source>
        <translation>Αφαίρεση του Driver</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="479"/>
        <location filename="ui_mainwindow.h" line="779"/>
        <source>Add Driver</source>
        <translation>Πρόσθεση του Driver</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="520"/>
        <location filename="ui_mainwindow.h" line="781"/>
        <source>About NDISwrapper</source>
        <translation>Σχετικά με το NDISwrapper</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="532"/>
        <location filename="ui_mainwindow.h" line="782"/>
        <source>Install NDISwrapper</source>
        <translation>Εγκατάσταση του NDISwrapper</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="543"/>
        <location filename="ui_mainwindow.h" line="783"/>
        <source>In order to use Windows drivers you need first to install NDISwrapper</source>
        <translation>Για να χρησιμοποιήσετε τα οδηγοί των Windows θα πρέπει πρώτα να εγκαταστήσετε το πακέτο NDISwrapper.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="559"/>
        <location filename="ui_mainwindow.h" line="784"/>
        <source>Uninstall NDISwrapper</source>
        <translation>Απεγκατάσταση του NDISwrapper</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="591"/>
        <location filename="ui_mainwindow.h" line="798"/>
        <source>Net diagnostics</source>
        <translation>Διάγνωση του διαδίκτυο</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="603"/>
        <location filename="ui_mainwindow.h" line="786"/>
        <source>Ping</source>
        <translation>Ping</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="609"/>
        <location filename="mainwindow.ui" line="726"/>
        <location filename="ui_mainwindow.h" line="787"/>
        <location filename="ui_mainwindow.h" line="793"/>
        <source>Target URL:</source>
        <translation>Στόχος URL:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="619"/>
        <location filename="ui_mainwindow.h" line="788"/>
        <source>Packets</source>
        <translation>Πακέτα</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="661"/>
        <location filename="mainwindow.ui" line="781"/>
        <location filename="ui_mainwindow.h" line="789"/>
        <location filename="ui_mainwindow.h" line="795"/>
        <source>Start</source>
        <translation>Ξεκινήστε</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="678"/>
        <location filename="mainwindow.ui" line="798"/>
        <location filename="ui_mainwindow.h" line="790"/>
        <location filename="ui_mainwindow.h" line="796"/>
        <source>Clear</source>
        <translation>Εκκαθάριση</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="692"/>
        <location filename="mainwindow.ui" line="812"/>
        <location filename="ui_mainwindow.h" line="791"/>
        <location filename="ui_mainwindow.h" line="797"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="720"/>
        <location filename="ui_mainwindow.h" line="792"/>
        <source>Traceroute</source>
        <translation>Traceroute</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="736"/>
        <location filename="ui_mainwindow.h" line="794"/>
        <source>Hops</source>
        <translation>Hops</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="868"/>
        <location filename="ui_mainwindow.h" line="799"/>
        <source>About...</source>
        <translation>Περί...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="891"/>
        <location filename="ui_mainwindow.h" line="803"/>
        <source>Help</source>
        <translation>Βοήθεια </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="959"/>
        <location filename="ui_mainwindow.h" line="805"/>
        <source>&amp;Close</source>
        <translation>&amp;Κλείσιμο</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="966"/>
        <location filename="ui_mainwindow.h" line="807"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="87"/>
        <source>IP address from router:</source>
        <translation>Διεύθυνση IP από το router:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="88"/>
        <source>External IP address:</source>
        <translation>Εξωτερική διεύθυνση IP:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="196"/>
        <location filename="mainwindow.cpp" line="210"/>
        <location filename="mainwindow.cpp" line="224"/>
        <source>&amp;Copy</source>
        <translation>&amp;Αντιγραφή</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="198"/>
        <location filename="mainwindow.cpp" line="212"/>
        <location filename="mainwindow.cpp" line="226"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="199"/>
        <location filename="mainwindow.cpp" line="213"/>
        <location filename="mainwindow.cpp" line="227"/>
        <source>Copy &amp;All</source>
        <translation>Αντιγραφή &amp;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="201"/>
        <location filename="mainwindow.cpp" line="215"/>
        <location filename="mainwindow.cpp" line="229"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="261"/>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Traceroute not installed</source>
        <translation>Traceroute δεν έχει εγκατασταθεί</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>Traceroute is not installed, do you want to install it now?</source>
        <translation>Traceroute δεν έχει εγκατασταθεί, Θέλετε να το εγκαταστήσετε τώρα ;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="269"/>
        <source>Traceroute hasn&apos;t been installed</source>
        <translation>Traceroute δεν έχει εγκατασταθεί</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="270"/>
        <source>Traceroute cannot be installed. This may mean you are using the LiveCD or you are unable to reach the software repository,</source>
        <translation>Traceroute δεν μπορεί να εγκατασταθεί. Μπορεί αυτό να σημαίνει ότι χρησιμοποιείτε το LiveCD ή δεν μπορείτε να φτάσετε στο αποθετήριο λογισμικού,</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="279"/>
        <source>Traceroute is not installed and no Internet connection could be detected so it cannot be installed</source>
        <translation>Traceroute δεν έχει εγκατασταθεί και δεν υπάρχει σύνδεση στο Internet, ώστε να μην μπορεί να εγκατασταθεί</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="285"/>
        <location filename="mainwindow.cpp" line="326"/>
        <source>No destination host</source>
        <translation>Δεν υπαρχει host προορισμού</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="286"/>
        <location filename="mainwindow.cpp" line="327"/>
        <source>Please fill in the destination host field</source>
        <translation>Παρακαλούμε συμπληρώστε το πεδίο host προορισμού</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="440"/>
        <source>Loaded Drivers</source>
        <translation>Φορτωμένο Driver </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="449"/>
        <source>Unloaded Drivers</source>
        <translation>Εκφορτωμένο Driver </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="464"/>
        <source>Blocked Drivers</source>
        <translation>Αποκλεισμένο drivers</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="486"/>
        <source>Blocked Broadcom Drivers</source>
        <translation>Αποκλεισμένο drivers του Broadcom</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="894"/>
        <source>enabled</source>
        <translation>ενεργοποιημένο</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="897"/>
        <source>disabled</source>
        <translation>απενεργοποιημένο</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="900"/>
        <source>WiFi hardware switch is off</source>
        <translation>Ο διακόπτης υλικού WiFi είναι απενεργοποιημένος</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="908"/>
        <source>Locate the Windows driver you want to add</source>
        <translation>Εντοπίστε το οδήγo των Windows που θέλετε να προσθέσετε</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="908"/>
        <source>Windows installation information file (*.inf)</source>
        <translation>Windows Installer αρχείο πληροφοριών (*.inf)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="939"/>
        <source>*.sys file not found</source>
        <translation>Το αρχείο *.sys  δε βρέθηκε</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="939"/>
        <source>The *.sys files must be in the same location as the *.inf file. %1 cannot be found</source>
        <translation>Τα αρχεία *.sys πρέπει να είναι στο ιδιο μερος το αρχειο * .inf. 1% δεν μπορεί να βρεθεί</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="948"/>
        <source>sys file reference not found</source>
        <translation>δε βρέθηκε το αρχείο sys</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="948"/>
        <source>The sys file for the given driver cannot be determined after parsing the inf file</source>
        <translation>Το αρχείο sys για το οδήγo δεν μπορεί να προσδιοριστεί μετά ανάλυση του αρχείου .inf</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="967"/>
        <source>Ndiswrapper driver removed.</source>
        <translation>Το οδηγό Ndiswrapper έχει αφαιρεθεί.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="975"/>
        <source>%1 Help</source>
        <translation>%1 Βοήθεια</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1012"/>
        <source>About %1</source>
        <translation>Περί %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1013"/>
        <source>Version: </source>
        <translation>Έκδοση:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1014"/>
        <source>Program for troubleshooting and configuring network for antiX Linux</source>
        <translation>Eφαρμογή για την αντιμετώπιση προβλημάτων δικτύου και τη διαμόρφωση.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1016"/>
        <source>Copyright (c) MEPIS LLC and MX Linux</source>
        <translation>Copyright (c) MEPIS LLC and MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1017"/>
        <source>%1 License</source>
        <translation>%1 Άδεια</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Άδεια</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Καταγραφή αλλαγών</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Κλείσιμο</translation>
    </message>
    <message>
        <location filename="main.cpp" line="44"/>
        <source>You must run this program as root.</source>
        <translation>Πρέπει να τρέξετε αυτή την εφαρμογή ως Root.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="509"/>
        <source>Ndiswrapper is not installed</source>
        <translation>Το Ndiswrapper δεν είναι εγκατεστημένο </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="521"/>
        <source>driver installed</source>
        <translation>Το driver εγκαταστάθηκε</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="529"/>
        <source> and in use by </source>
        <translation>και σε χρήση από</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="536"/>
        <source>. Alternate driver: </source>
        <translation>Εναλλακτικό driver: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="613"/>
        <source>Driver removed from blocklist</source>
        <translation>Το driver καταργήθηκε από τη λίστα αποκλεισμού</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="614"/>
        <source>Driver removed from blocklist.</source>
        <translation>Το driver καταργήθηκε από τη λίστα αποκλεισμού</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="620"/>
        <location filename="mainwindow.cpp" line="621"/>
        <source>Module blocked</source>
        <translation>Αποκλεισμένο module</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="786"/>
        <source>Installation successful</source>
        <translation>Εγκατάσταση με επιτυχια</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="789"/>
        <source>Error detected, could not compile ndiswrapper driver.</source>
        <translation>Εντοπίστηκε σφάλμα, δεν θα μπορούσαν να καταρτίσουν πρόγραμμα οδήγησης ndiswrapper.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="792"/>
        <source>Error detected, could not install ndiswrapper.</source>
        <translation>Εντοπίστηκε σφάλμα, δεν θα μπορούσε να εγκαταστήσετε το πακέτο ndiswrapper.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="804"/>
        <source>Error encountered while removing Ndiswrapper</source>
        <translation>Προέκυψε σφάλμα κατά την αφαίρεση του ndiswrapper</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="855"/>
        <source>Unblock Driver</source>
        <translation>Unblock Driver</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="858"/>
        <source>Block Driver</source>
        <translation>Αποκλεισμός του driver</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="995"/>
        <source>Could not unlock devices.
WiFi device(s) might already be unlocked.</source>
        <translation>Δεν θα μπορούσε να ξεκλειδώσει τις συσκευές.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="997"/>
        <source>WiFi devices unlocked.</source>
        <translation>Συσκευές WiFi εχει ξεκλειδωθεί.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1040"/>
        <location filename="mainwindow.cpp" line="1041"/>
        <source>Driver loaded successfully</source>
        <translation>Το Driver φορτώθηκε επιτυχώς</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1056"/>
        <location filename="mainwindow.cpp" line="1057"/>
        <source>Driver unloaded successfully</source>
        <translation>Το Driver ξεφορτώθηκε επιτυχώς
</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="mainwindow.cpp" line="638"/>
        <source>Could not load </source>
        <translation>Δεν ήταν δυνατή η φόρτωση</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="669"/>
        <source>Could not unload </source>
        <translation>Δεν ήταν δυνατή η ξεφόρτωση</translation>
    </message>
</context>
</TS>