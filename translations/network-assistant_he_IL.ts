<?xml version="1.0" ?><!DOCTYPE TS><TS language="he_IL" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <location filename="ui_mainwindow.h" line="756"/>
        <source>Network Assistant</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="70"/>
        <location filename="ui_mainwindow.h" line="770"/>
        <source>Status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="76"/>
        <location filename="ui_mainwindow.h" line="757"/>
        <source>IP address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="112"/>
        <location filename="ui_mainwindow.h" line="760"/>
        <source>Hardware detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="143"/>
        <location filename="mainwindow.ui" line="389"/>
        <location filename="mainwindow.ui" line="502"/>
        <location filename="ui_mainwindow.h" line="761"/>
        <location filename="ui_mainwindow.h" line="774"/>
        <location filename="ui_mainwindow.h" line="780"/>
        <source>Re-scan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="215"/>
        <location filename="ui_mainwindow.h" line="762"/>
        <source>Active interface</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="231"/>
        <location filename="ui_mainwindow.h" line="764"/>
        <source>WiFi status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="253"/>
        <location filename="ui_mainwindow.h" line="767"/>
        <source>Unblocks all soft/hard blocked wireless devices</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="256"/>
        <location filename="ui_mainwindow.h" line="769"/>
        <source>Unblock WiFi Devices</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="288"/>
        <location filename="ui_mainwindow.h" line="776"/>
        <source>Linux drivers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="300"/>
        <location filename="ui_mainwindow.h" line="771"/>
        <source>Associated Linux drivers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="309"/>
        <location filename="ui_mainwindow.h" line="772"/>
        <source>Load Driver</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="372"/>
        <location filename="ui_mainwindow.h" line="773"/>
        <source>Unload Driver</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="409"/>
        <location filename="ui_mainwindow.h" line="775"/>
        <source>Block Driver</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="428"/>
        <location filename="ui_mainwindow.h" line="785"/>
        <source>Windows drivers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="434"/>
        <location filename="ui_mainwindow.h" line="777"/>
        <source>Available Windows drivers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="468"/>
        <location filename="ui_mainwindow.h" line="778"/>
        <source>Remove Driver</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="479"/>
        <location filename="ui_mainwindow.h" line="779"/>
        <source>Add Driver</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="520"/>
        <location filename="ui_mainwindow.h" line="781"/>
        <source>About NDISwrapper</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="532"/>
        <location filename="ui_mainwindow.h" line="782"/>
        <source>Install NDISwrapper</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="543"/>
        <location filename="ui_mainwindow.h" line="783"/>
        <source>In order to use Windows drivers you need first to install NDISwrapper</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="559"/>
        <location filename="ui_mainwindow.h" line="784"/>
        <source>Uninstall NDISwrapper</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="591"/>
        <location filename="ui_mainwindow.h" line="798"/>
        <source>Net diagnostics</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="603"/>
        <location filename="ui_mainwindow.h" line="786"/>
        <source>Ping</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="609"/>
        <location filename="mainwindow.ui" line="726"/>
        <location filename="ui_mainwindow.h" line="787"/>
        <location filename="ui_mainwindow.h" line="793"/>
        <source>Target URL:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="619"/>
        <location filename="ui_mainwindow.h" line="788"/>
        <source>Packets</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="661"/>
        <location filename="mainwindow.ui" line="781"/>
        <location filename="ui_mainwindow.h" line="789"/>
        <location filename="ui_mainwindow.h" line="795"/>
        <source>Start</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="678"/>
        <location filename="mainwindow.ui" line="798"/>
        <location filename="ui_mainwindow.h" line="790"/>
        <location filename="ui_mainwindow.h" line="796"/>
        <source>Clear</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="692"/>
        <location filename="mainwindow.ui" line="812"/>
        <location filename="ui_mainwindow.h" line="791"/>
        <location filename="ui_mainwindow.h" line="797"/>
        <source>Cancel</source>
        <translation>ביטול</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="720"/>
        <location filename="ui_mainwindow.h" line="792"/>
        <source>Traceroute</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="736"/>
        <location filename="ui_mainwindow.h" line="794"/>
        <source>Hops</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="868"/>
        <location filename="ui_mainwindow.h" line="799"/>
        <source>About...</source>
        <translation>אודות...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="891"/>
        <location filename="ui_mainwindow.h" line="803"/>
        <source>Help</source>
        <translation>עזרה</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="959"/>
        <location filename="ui_mainwindow.h" line="805"/>
        <source>&amp;Close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="966"/>
        <location filename="ui_mainwindow.h" line="807"/>
        <source>Alt+C</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="87"/>
        <source>IP address from router:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="88"/>
        <source>External IP address:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="196"/>
        <location filename="mainwindow.cpp" line="210"/>
        <location filename="mainwindow.cpp" line="224"/>
        <source>&amp;Copy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="198"/>
        <location filename="mainwindow.cpp" line="212"/>
        <location filename="mainwindow.cpp" line="226"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="199"/>
        <location filename="mainwindow.cpp" line="213"/>
        <location filename="mainwindow.cpp" line="227"/>
        <source>Copy &amp;All</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="201"/>
        <location filename="mainwindow.cpp" line="215"/>
        <location filename="mainwindow.cpp" line="229"/>
        <source>Ctrl+A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="261"/>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Traceroute not installed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>Traceroute is not installed, do you want to install it now?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="269"/>
        <source>Traceroute hasn&apos;t been installed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="270"/>
        <source>Traceroute cannot be installed. This may mean you are using the LiveCD or you are unable to reach the software repository,</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="279"/>
        <source>Traceroute is not installed and no Internet connection could be detected so it cannot be installed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="285"/>
        <location filename="mainwindow.cpp" line="326"/>
        <source>No destination host</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="286"/>
        <location filename="mainwindow.cpp" line="327"/>
        <source>Please fill in the destination host field</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="440"/>
        <source>Loaded Drivers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="449"/>
        <source>Unloaded Drivers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="464"/>
        <source>Blocked Drivers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="486"/>
        <source>Blocked Broadcom Drivers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="894"/>
        <source>enabled</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="897"/>
        <source>disabled</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="900"/>
        <source>WiFi hardware switch is off</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="908"/>
        <source>Locate the Windows driver you want to add</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="908"/>
        <source>Windows installation information file (*.inf)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="939"/>
        <source>*.sys file not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="939"/>
        <source>The *.sys files must be in the same location as the *.inf file. %1 cannot be found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="948"/>
        <source>sys file reference not found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="948"/>
        <source>The sys file for the given driver cannot be determined after parsing the inf file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="967"/>
        <source>Ndiswrapper driver removed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="975"/>
        <source>%1 Help</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1012"/>
        <source>About %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1013"/>
        <source>Version: </source>
        <translation>גרסה:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1014"/>
        <source>Program for troubleshooting and configuring network for antiX Linux</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1016"/>
        <source>Copyright (c) MEPIS LLC and MX Linux</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1017"/>
        <source>%1 License</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>רשיון</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>ביטול</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="main.cpp" line="44"/>
        <source>You must run this program as root.</source>
        <translation>אתה חייב להריץ את התוכנה הזו עם הרשאות root.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="509"/>
        <source>Ndiswrapper is not installed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="521"/>
        <source>driver installed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="529"/>
        <source> and in use by </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="536"/>
        <source>. Alternate driver: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="613"/>
        <source>Driver removed from blocklist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="614"/>
        <source>Driver removed from blocklist.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="620"/>
        <location filename="mainwindow.cpp" line="621"/>
        <source>Module blocked</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="786"/>
        <source>Installation successful</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="789"/>
        <source>Error detected, could not compile ndiswrapper driver.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="792"/>
        <source>Error detected, could not install ndiswrapper.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="804"/>
        <source>Error encountered while removing Ndiswrapper</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="855"/>
        <source>Unblock Driver</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="858"/>
        <source>Block Driver</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="995"/>
        <source>Could not unlock devices.
WiFi device(s) might already be unlocked.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="997"/>
        <source>WiFi devices unlocked.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1040"/>
        <location filename="mainwindow.cpp" line="1041"/>
        <source>Driver loaded successfully</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1056"/>
        <location filename="mainwindow.cpp" line="1057"/>
        <source>Driver unloaded successfully</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="mainwindow.cpp" line="638"/>
        <source>Could not load </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="669"/>
        <source>Could not unload </source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>