<?xml version="1.0" ?><!DOCTYPE TS><TS language="da" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <location filename="ui_mainwindow.h" line="756"/>
        <source>Network Assistant</source>
        <translation>Netværksassistent</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="70"/>
        <location filename="ui_mainwindow.h" line="770"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="76"/>
        <location filename="ui_mainwindow.h" line="757"/>
        <source>IP address</source>
        <translation>IP-adresse</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="112"/>
        <location filename="ui_mainwindow.h" line="760"/>
        <source>Hardware detected</source>
        <translation>Hardwareregistrering</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="143"/>
        <location filename="mainwindow.ui" line="389"/>
        <location filename="mainwindow.ui" line="502"/>
        <location filename="ui_mainwindow.h" line="761"/>
        <location filename="ui_mainwindow.h" line="774"/>
        <location filename="ui_mainwindow.h" line="780"/>
        <source>Re-scan</source>
        <translation>Skan igen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="215"/>
        <location filename="ui_mainwindow.h" line="762"/>
        <source>Active interface</source>
        <translation>Aktiv grænseflade</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="231"/>
        <location filename="ui_mainwindow.h" line="764"/>
        <source>WiFi status</source>
        <translation>WiFi-status</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="253"/>
        <location filename="ui_mainwindow.h" line="767"/>
        <source>Unblocks all soft/hard blocked wireless devices</source>
        <translation>Afblokerer alle blødt/hårdt blokerede trådløse enheder</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="256"/>
        <location filename="ui_mainwindow.h" line="769"/>
        <source>Unblock WiFi Devices</source>
        <translation>Afbloker WiFi-enheder</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="288"/>
        <location filename="ui_mainwindow.h" line="776"/>
        <source>Linux drivers</source>
        <translation>Linux-drivere</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="300"/>
        <location filename="ui_mainwindow.h" line="771"/>
        <source>Associated Linux drivers</source>
        <translation>Tilknyttede Linux-drivere</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="309"/>
        <location filename="ui_mainwindow.h" line="772"/>
        <source>Load Driver</source>
        <translation>Indlæs driver</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="372"/>
        <location filename="ui_mainwindow.h" line="773"/>
        <source>Unload Driver</source>
        <translation>Afindlæs driver</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="409"/>
        <location filename="ui_mainwindow.h" line="775"/>
        <source>Block Driver</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.ui" line="428"/>
        <location filename="ui_mainwindow.h" line="785"/>
        <source>Windows drivers</source>
        <translation>Windows-drivere</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="434"/>
        <location filename="ui_mainwindow.h" line="777"/>
        <source>Available Windows drivers</source>
        <translation>Tilgængelige Windows-drivere</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="468"/>
        <location filename="ui_mainwindow.h" line="778"/>
        <source>Remove Driver</source>
        <translation>Fjern driver</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="479"/>
        <location filename="ui_mainwindow.h" line="779"/>
        <source>Add Driver</source>
        <translation>Tilføj driver</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="520"/>
        <location filename="ui_mainwindow.h" line="781"/>
        <source>About NDISwrapper</source>
        <translation>Om NDISwrapper</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="532"/>
        <location filename="ui_mainwindow.h" line="782"/>
        <source>Install NDISwrapper</source>
        <translation>Installer NDISwrapper</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="543"/>
        <location filename="ui_mainwindow.h" line="783"/>
        <source>In order to use Windows drivers you need first to install NDISwrapper</source>
        <translation>For at bruge Windows-drivere, skal du først installere NDISwrapper</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="559"/>
        <location filename="ui_mainwindow.h" line="784"/>
        <source>Uninstall NDISwrapper</source>
        <translation>Afinstaller NDISwrapper</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="591"/>
        <location filename="ui_mainwindow.h" line="798"/>
        <source>Net diagnostics</source>
        <translation>Netdiagnostisk</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="603"/>
        <location filename="ui_mainwindow.h" line="786"/>
        <source>Ping</source>
        <translation>Ping</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="609"/>
        <location filename="mainwindow.ui" line="726"/>
        <location filename="ui_mainwindow.h" line="787"/>
        <location filename="ui_mainwindow.h" line="793"/>
        <source>Target URL:</source>
        <translation>Mål-URL:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="619"/>
        <location filename="ui_mainwindow.h" line="788"/>
        <source>Packets</source>
        <translation>Pakker</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="661"/>
        <location filename="mainwindow.ui" line="781"/>
        <location filename="ui_mainwindow.h" line="789"/>
        <location filename="ui_mainwindow.h" line="795"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="678"/>
        <location filename="mainwindow.ui" line="798"/>
        <location filename="ui_mainwindow.h" line="790"/>
        <location filename="ui_mainwindow.h" line="796"/>
        <source>Clear</source>
        <translation>Ryd</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="692"/>
        <location filename="mainwindow.ui" line="812"/>
        <location filename="ui_mainwindow.h" line="791"/>
        <location filename="ui_mainwindow.h" line="797"/>
        <source>Cancel</source>
        <translation>Annuller</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="720"/>
        <location filename="ui_mainwindow.h" line="792"/>
        <source>Traceroute</source>
        <translation>Traceroute</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="736"/>
        <location filename="ui_mainwindow.h" line="794"/>
        <source>Hops</source>
        <translation>Hops</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="868"/>
        <location filename="ui_mainwindow.h" line="799"/>
        <source>About...</source>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="891"/>
        <location filename="ui_mainwindow.h" line="803"/>
        <source>Help</source>
        <translation>Hjælp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="959"/>
        <location filename="ui_mainwindow.h" line="805"/>
        <source>&amp;Close</source>
        <translation>&amp;Luk</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="966"/>
        <location filename="ui_mainwindow.h" line="807"/>
        <source>Alt+C</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="87"/>
        <source>IP address from router:</source>
        <translation>IP-adresse fra router:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="88"/>
        <source>External IP address:</source>
        <translation>Ekstern IP-adresse:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="196"/>
        <location filename="mainwindow.cpp" line="210"/>
        <location filename="mainwindow.cpp" line="224"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopiér</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="198"/>
        <location filename="mainwindow.cpp" line="212"/>
        <location filename="mainwindow.cpp" line="226"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="199"/>
        <location filename="mainwindow.cpp" line="213"/>
        <location filename="mainwindow.cpp" line="227"/>
        <source>Copy &amp;All</source>
        <translation>Kopiér &amp;alle</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="201"/>
        <location filename="mainwindow.cpp" line="215"/>
        <location filename="mainwindow.cpp" line="229"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="261"/>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Traceroute not installed</source>
        <translation>Traceroute ikke installeret</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>Traceroute is not installed, do you want to install it now?</source>
        <translation>Traceroute er ikke installeret, vil du installere den nu?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="269"/>
        <source>Traceroute hasn&apos;t been installed</source>
        <translation>Traceroute blev ikke installeret</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="270"/>
        <source>Traceroute cannot be installed. This may mean you are using the LiveCD or you are unable to reach the software repository,</source>
        <translation>Traceroute kan ikke installeres. Det kan betyde at du bruger LiveCD eller at du ikke kan nå softwarekilden,</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="279"/>
        <source>Traceroute is not installed and no Internet connection could be detected so it cannot be installed</source>
        <translation>Traceroute er ikke installeret og der kunne ikke registreres nogen internetforbindelse, så den kan ikke installeres</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="285"/>
        <location filename="mainwindow.cpp" line="326"/>
        <source>No destination host</source>
        <translation>Ingen destinationsvært</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="286"/>
        <location filename="mainwindow.cpp" line="327"/>
        <source>Please fill in the destination host field</source>
        <translation>Udfyld venligst destinationsvært-feltet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="440"/>
        <source>Loaded Drivers</source>
        <translation>Indlæste drivere</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="449"/>
        <source>Unloaded Drivers</source>
        <translation>Afindlæste drivere</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="464"/>
        <source>Blocked Drivers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="486"/>
        <source>Blocked Broadcom Drivers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="894"/>
        <source>enabled</source>
        <translation>aktivér</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="897"/>
        <source>disabled</source>
        <translation>deaktivér</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="900"/>
        <source>WiFi hardware switch is off</source>
        <translation>WiFi hardwarekontakten er slukket</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="908"/>
        <source>Locate the Windows driver you want to add</source>
        <translation>Find den Windows-driver du vil tilføje</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="908"/>
        <source>Windows installation information file (*.inf)</source>
        <translation>Windows-installationsinformation-fil (*.inf)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="939"/>
        <source>*.sys file not found</source>
        <translation>*.sys-fil ikke fundet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="939"/>
        <source>The *.sys files must be in the same location as the *.inf file. %1 cannot be found</source>
        <translation>*.sys-filedrne skal være i den samme placering som *.inf-filen. Kan ikke finde %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="948"/>
        <source>sys file reference not found</source>
        <translation>sys-filreference ikke fundet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="948"/>
        <source>The sys file for the given driver cannot be determined after parsing the inf file</source>
        <translation>Sys-filen til den givne driver kan ikke bestemmes efter fortolkning af inf-filen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="967"/>
        <source>Ndiswrapper driver removed.</source>
        <translation>Ndiswrapper-driver fjernet.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="975"/>
        <source>%1 Help</source>
        <translation>%1-hjælp</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1012"/>
        <source>About %1</source>
        <translation>Om %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1013"/>
        <source>Version: </source>
        <translation>Version: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1014"/>
        <source>Program for troubleshooting and configuring network for antiX Linux</source>
        <translation>Program til at fejlsøge og konfigurere netværk til antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1016"/>
        <source>Copyright (c) MEPIS LLC and MX Linux</source>
        <translation>Ophavsret (c) MEPIS LLC og MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1017"/>
        <source>%1 License</source>
        <translation>%1-licens</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Licens</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Ændringslog</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Annuller</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Luk</translation>
    </message>
    <message>
        <location filename="main.cpp" line="44"/>
        <source>You must run this program as root.</source>
        <translation>Du skal køre programmet som root.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="509"/>
        <source>Ndiswrapper is not installed</source>
        <translation>Ndiswrapper er ikke installeret</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="521"/>
        <source>driver installed</source>
        <translation>driver installeret</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="529"/>
        <source> and in use by </source>
        <translation>og i brug af</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="536"/>
        <source>. Alternate driver: </source>
        <translation>. Alternativ driver: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="613"/>
        <source>Driver removed from blocklist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="614"/>
        <source>Driver removed from blocklist.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="620"/>
        <location filename="mainwindow.cpp" line="621"/>
        <source>Module blocked</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="786"/>
        <source>Installation successful</source>
        <translation>Installation lykkedes</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="789"/>
        <source>Error detected, could not compile ndiswrapper driver.</source>
        <translation>Fejl registreret, kunne ikke kompilere ndiswrapper-driver.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="792"/>
        <source>Error detected, could not install ndiswrapper.</source>
        <translation>Fejl registreret, kunne ikke installere ndiswrapper.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="804"/>
        <source>Error encountered while removing Ndiswrapper</source>
        <translation>Fejl registreret under fjernelse af Ndiswrapper</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="855"/>
        <source>Unblock Driver</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="858"/>
        <source>Block Driver</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="995"/>
        <source>Could not unlock devices.
WiFi device(s) might already be unlocked.</source>
        <translation>Kunne ikke afblokere enheder.
WiFi-enhed(er) er måske allerede låst op.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="997"/>
        <source>WiFi devices unlocked.</source>
        <translation>WiFi-enheder låst op.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1040"/>
        <location filename="mainwindow.cpp" line="1041"/>
        <source>Driver loaded successfully</source>
        <translation>Driver indlæst</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1056"/>
        <location filename="mainwindow.cpp" line="1057"/>
        <source>Driver unloaded successfully</source>
        <translation>Driver afindlæst</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="mainwindow.cpp" line="638"/>
        <source>Could not load </source>
        <translation>Kunne ikke indlæse</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="669"/>
        <source>Could not unload </source>
        <translation>Kunne ikke afindlæse</translation>
    </message>
</context>
</TS>