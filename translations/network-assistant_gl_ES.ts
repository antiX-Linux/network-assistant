<?xml version="1.0" ?><!DOCTYPE TS><TS language="gl_ES" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <location filename="ui_mainwindow.h" line="756"/>
        <source>Network Assistant</source>
        <translation>Asistente da rede</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="70"/>
        <location filename="ui_mainwindow.h" line="770"/>
        <source>Status</source>
        <translation>Estado</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="76"/>
        <location filename="ui_mainwindow.h" line="757"/>
        <source>IP address</source>
        <translation>Enderezo IP</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="112"/>
        <location filename="ui_mainwindow.h" line="760"/>
        <source>Hardware detected</source>
        <translation>Dispositivo detectado</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="143"/>
        <location filename="mainwindow.ui" line="389"/>
        <location filename="mainwindow.ui" line="502"/>
        <location filename="ui_mainwindow.h" line="761"/>
        <location filename="ui_mainwindow.h" line="774"/>
        <location filename="ui_mainwindow.h" line="780"/>
        <source>Re-scan</source>
        <translation>Volver a buscar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="215"/>
        <location filename="ui_mainwindow.h" line="762"/>
        <source>Active interface</source>
        <translation>Interface activada</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="231"/>
        <location filename="ui_mainwindow.h" line="764"/>
        <source>WiFi status</source>
        <translation>Estado do WiFi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="253"/>
        <location filename="ui_mainwindow.h" line="767"/>
        <source>Unblocks all soft/hard blocked wireless devices</source>
        <translation>Desbloquea todos os dispositivos sen fíos bloqueados por software ou no equipamento</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="256"/>
        <location filename="ui_mainwindow.h" line="769"/>
        <source>Unblock WiFi Devices</source>
        <translation>Desbloquear dispositivos WiFi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="288"/>
        <location filename="ui_mainwindow.h" line="776"/>
        <source>Linux drivers</source>
        <translation>COntroladores Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="300"/>
        <location filename="ui_mainwindow.h" line="771"/>
        <source>Associated Linux drivers</source>
        <translation>Controladores Linux asociados</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="309"/>
        <location filename="ui_mainwindow.h" line="772"/>
        <source>Load Driver</source>
        <translation>Instalar controlador</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="372"/>
        <location filename="ui_mainwindow.h" line="773"/>
        <source>Unload Driver</source>
        <translation>Desinstalar controlador</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="409"/>
        <location filename="ui_mainwindow.h" line="775"/>
        <source>Block Driver</source>
        <translation>Bloquear controlador</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="428"/>
        <location filename="ui_mainwindow.h" line="785"/>
        <source>Windows drivers</source>
        <translation>Controladores do Windows</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="434"/>
        <location filename="ui_mainwindow.h" line="777"/>
        <source>Available Windows drivers</source>
        <translation>Controladores Windows dispoñibles</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="468"/>
        <location filename="ui_mainwindow.h" line="778"/>
        <source>Remove Driver</source>
        <translation>Eliminar controlador</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="479"/>
        <location filename="ui_mainwindow.h" line="779"/>
        <source>Add Driver</source>
        <translation>Engadir controlador</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="520"/>
        <location filename="ui_mainwindow.h" line="781"/>
        <source>About NDISwrapper</source>
        <translation>Sobre NDISwrapper</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="532"/>
        <location filename="ui_mainwindow.h" line="782"/>
        <source>Install NDISwrapper</source>
        <translation>Instalar NDISwrapper</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="543"/>
        <location filename="ui_mainwindow.h" line="783"/>
        <source>In order to use Windows drivers you need first to install NDISwrapper</source>
        <translation>Para usar controladores Windows é necesario instalar primeiro NDISwrapper</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="559"/>
        <location filename="ui_mainwindow.h" line="784"/>
        <source>Uninstall NDISwrapper</source>
        <translation>Desinstalar NDISwrapper</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="591"/>
        <location filename="ui_mainwindow.h" line="798"/>
        <source>Net diagnostics</source>
        <translation>Diagnósticos da rede</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="603"/>
        <location filename="ui_mainwindow.h" line="786"/>
        <source>Ping</source>
        <translation>Ping</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="609"/>
        <location filename="mainwindow.ui" line="726"/>
        <location filename="ui_mainwindow.h" line="787"/>
        <location filename="ui_mainwindow.h" line="793"/>
        <source>Target URL:</source>
        <translation>URL ligada:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="619"/>
        <location filename="ui_mainwindow.h" line="788"/>
        <source>Packets</source>
        <translation>Paquetes</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="661"/>
        <location filename="mainwindow.ui" line="781"/>
        <location filename="ui_mainwindow.h" line="789"/>
        <location filename="ui_mainwindow.h" line="795"/>
        <source>Start</source>
        <translation>Iniciar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="678"/>
        <location filename="mainwindow.ui" line="798"/>
        <location filename="ui_mainwindow.h" line="790"/>
        <location filename="ui_mainwindow.h" line="796"/>
        <source>Clear</source>
        <translation>Limpar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="692"/>
        <location filename="mainwindow.ui" line="812"/>
        <location filename="ui_mainwindow.h" line="791"/>
        <location filename="ui_mainwindow.h" line="797"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="720"/>
        <location filename="ui_mainwindow.h" line="792"/>
        <source>Traceroute</source>
        <translation>Traceroute</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="736"/>
        <location filename="ui_mainwindow.h" line="794"/>
        <source>Hops</source>
        <translation>Saltos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="868"/>
        <location filename="ui_mainwindow.h" line="799"/>
        <source>About...</source>
        <translation>Sobre...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="891"/>
        <location filename="ui_mainwindow.h" line="803"/>
        <source>Help</source>
        <translation>Axuda</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="959"/>
        <location filename="ui_mainwindow.h" line="805"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="966"/>
        <location filename="ui_mainwindow.h" line="807"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="87"/>
        <source>IP address from router:</source>
        <translation>Enderezo IP do rúter:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="88"/>
        <source>External IP address:</source>
        <translation>Enderezo IP externo:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="196"/>
        <location filename="mainwindow.cpp" line="210"/>
        <location filename="mainwindow.cpp" line="224"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copiar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="198"/>
        <location filename="mainwindow.cpp" line="212"/>
        <location filename="mainwindow.cpp" line="226"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="199"/>
        <location filename="mainwindow.cpp" line="213"/>
        <location filename="mainwindow.cpp" line="227"/>
        <source>Copy &amp;All</source>
        <translation>Copiar &amp;Todo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="201"/>
        <location filename="mainwindow.cpp" line="215"/>
        <location filename="mainwindow.cpp" line="229"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="261"/>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Traceroute not installed</source>
        <translation>Traceroute non está instalado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="262"/>
        <source>Traceroute is not installed, do you want to install it now?</source>
        <translation>Traceroute non está instalado, instalar agora?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="269"/>
        <source>Traceroute hasn&apos;t been installed</source>
        <translation>Traceroute non foi instalado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="270"/>
        <source>Traceroute cannot be installed. This may mean you are using the LiveCD or you are unable to reach the software repository,</source>
        <translation>Non foi posible instalar Traceroute. Isto pode deberse a estar usando a instalación externa ou de non conseguir conectalo ao repositorio do software.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="279"/>
        <source>Traceroute is not installed and no Internet connection could be detected so it cannot be installed</source>
        <translation>Traceroute non está instalado. Non foi detectada ningunha conexión a Internet, polo que non é posible proceder á súa instalación</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="285"/>
        <location filename="mainwindow.cpp" line="326"/>
        <source>No destination host</source>
        <translation>Nongún hospedeiro como destino</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="286"/>
        <location filename="mainwindow.cpp" line="327"/>
        <source>Please fill in the destination host field</source>
        <translation>Enche o campo do hospedeiro de destin</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="440"/>
        <source>Loaded Drivers</source>
        <translation>Controladores cargados</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="449"/>
        <source>Unloaded Drivers</source>
        <translation>Controladores non cargados</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="464"/>
        <source>Blocked Drivers</source>
        <translation>Controladores bloqueados</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="486"/>
        <source>Blocked Broadcom Drivers</source>
        <translation>Controladores Broadcom bloqueados</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="894"/>
        <source>enabled</source>
        <translation>activado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="897"/>
        <source>disabled</source>
        <translation>desactivadoq</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="900"/>
        <source>WiFi hardware switch is off</source>
        <translation>O interruptor de WiFi do computador está apagado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="908"/>
        <source>Locate the Windows driver you want to add</source>
        <translation>Localizar o controlador Windows a engadir</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="908"/>
        <source>Windows installation information file (*.inf)</source>
        <translation>Ficheiro de información sobre a instalación Windows (*.inf)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="939"/>
        <source>*.sys file not found</source>
        <translation>Non foi atopado o ficheiro *.sys</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="939"/>
        <source>The *.sys files must be in the same location as the *.inf file. %1 cannot be found</source>
        <translation>Os ficheiros *.sys teñen que estar no mesmo local que o ficheiro *.inf. Non foi atopado o ficheiro %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="948"/>
        <source>sys file reference not found</source>
        <translation>Non foi atopada a referencia do ficheiro sys</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="948"/>
        <source>The sys file for the given driver cannot be determined after parsing the inf file</source>
        <translation>A análise sintáctica ao ficheiro inf non permitiu determinar cal é o ficheiro sys para o controlador en cuestión</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="967"/>
        <source>Ndiswrapper driver removed.</source>
        <translation>O controlador do NDISwrapper foi eliminado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="975"/>
        <source>%1 Help</source>
        <translation>Axuda para %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1012"/>
        <source>About %1</source>
        <translation>Sobre %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1013"/>
        <source>Version: </source>
        <translation>Versión</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1014"/>
        <source>Program for troubleshooting and configuring network for antiX Linux</source>
        <translation>Programa para a solución de problemas e configuración da rede para o antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1016"/>
        <source>Copyright (c) MEPIS LLC and MX Linux</source>
        <translation>Copyright (c) MEPIS LLC e MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1017"/>
        <source>%1 License</source>
        <translation>Licenza de %1</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="32"/>
        <source>License</source>
        <translation>Licenza</translation>
    </message>
    <message>
        <location filename="about.cpp" line="33"/>
        <location filename="about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Rexistro dos cambios</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <location filename="main.cpp" line="44"/>
        <source>You must run this program as root.</source>
        <translation>Este programa ten que ser executado como administrador.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="509"/>
        <source>Ndiswrapper is not installed</source>
        <translation>NDISwrapper non está instalado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="521"/>
        <source>driver installed</source>
        <translation>controlador instalado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="529"/>
        <source> and in use by </source>
        <translation>e en uso por</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="536"/>
        <source>. Alternate driver: </source>
        <translation>.Controlador alternativo:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="613"/>
        <source>Driver removed from blocklist</source>
        <translation>Controlador eliminado da lista de bloqueados</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="614"/>
        <source>Driver removed from blocklist.</source>
        <translation>Controlador eliminado da lista de bloqueados.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="620"/>
        <location filename="mainwindow.cpp" line="621"/>
        <source>Module blocked</source>
        <translation>Módulo bloqueado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="786"/>
        <source>Installation successful</source>
        <translation>Instalación exitosa</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="789"/>
        <source>Error detected, could not compile ndiswrapper driver.</source>
        <translation>Detectado un erro, non foi posible compilar o controlador do ndiswrapper.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="792"/>
        <source>Error detected, could not install ndiswrapper.</source>
        <translation>Detectado un erro, non foi posible instalar ndiswrapper.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="804"/>
        <source>Error encountered while removing Ndiswrapper</source>
        <translation>Detectado un erro ao eliminar Ndiswrapper</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="855"/>
        <source>Unblock Driver</source>
        <translation>Desbloquear controlador</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="858"/>
        <source>Block Driver</source>
        <translation>Bloquear controlador</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="995"/>
        <source>Could not unlock devices.
WiFi device(s) might already be unlocked.</source>
        <translation>Non foi posible desbloquear os dispositivos.
Os dispostivo(s) WiFi poden estar xa desbloqueados.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="997"/>
        <source>WiFi devices unlocked.</source>
        <translation>Dispositivos WiFi desbloqueados.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1040"/>
        <location filename="mainwindow.cpp" line="1041"/>
        <source>Driver loaded successfully</source>
        <translation>Controlador cargado exitosamente</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1056"/>
        <location filename="mainwindow.cpp" line="1057"/>
        <source>Driver unloaded successfully</source>
        <translation>Controlador descargado con éxito</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="mainwindow.cpp" line="638"/>
        <source>Could not load </source>
        <translation>Non foi posible cargar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="669"/>
        <source>Could not unload </source>
        <translation>Non foi posible descargar</translation>
    </message>
</context>
</TS>