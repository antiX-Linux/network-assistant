greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app
TARGET = network-assistant

TRANSLATIONS += translations/network-assistant_am.ts \
                translations/network-assistant_ar.ts \
                translations/network-assistant_bg.ts \
                translations/network-assistant_ca.ts \
                translations/network-assistant_cs.ts \
                translations/network-assistant_da.ts \
                translations/network-assistant_de.ts \
                translations/network-assistant_el.ts \
                translations/network-assistant_es.ts \
                translations/network-assistant_et.ts \
                translations/network-assistant_eu.ts \
                translations/network-assistant_fa.ts \
                translations/network-assistant_fil_PH.ts \
                translations/network-assistant_fi.ts \
                translations/network-assistant_fr.ts \
                translations/network-assistant_gl_ES.ts \
                translations/network-assistant_he_IL.ts \
                translations/network-assistant_hi.ts \
                translations/network-assistant_hr.ts \
                translations/network-assistant_hu.ts \
                translations/network-assistant_id.ts \
                translations/network-assistant_is.ts \
                translations/network-assistant_it.ts \
                translations/network-assistant_ja.ts \
                translations/network-assistant_kk.ts \
                translations/network-assistant_ko.ts \
                translations/network-assistant_lt.ts \
                translations/network-assistant_mk.ts \
                translations/network-assistant_nb.ts \
                translations/network-assistant_nl.ts \
                translations/network-assistant_pl.ts \
                translations/network-assistant_pt.ts \
                translations/network-assistant_pt_BR.ts \
                translations/network-assistant_ro.ts \
                translations/network-assistant_ru.ts \
                translations/network-assistant_sk.ts \
                translations/network-assistant_sl.ts \
                translations/network-assistant_sq.ts \
                translations/network-assistant_sr.ts \
                translations/network-assistant_sv.ts \
                translations/network-assistant_tr.ts \
                translations/network-assistant_uk.ts \
                translations/network-assistant_zh_CN.ts \
                translations/network-assistant_zh_TW.ts

FORMS += \
    mainwindow.ui
HEADERS += \
    mainwindow.h \
    version.h \
    about.h \
    cmd.h
SOURCES += main.cpp \
    mainwindow.cpp \
    about.cpp \
    cmd.cpp
CONFIG += release warn_on thread qt c++11

RESOURCES += \
    images.qrc

